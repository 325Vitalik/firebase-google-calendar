import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';

const yourFirebaseConfig = {
  apiKey: 'AIzaSyCOuwbwANkbbZT5hSpwpIDvXdZuce9fhZc',
  authDomain: 'test-gapi-vy.firebaseapp.com',
  databaseURL: 'https://test-gapi-vy.firebaseio.com',
  projectId: 'test-gapi-vy',
  storageBucket: 'test-gapi-vy.appspot.com',
  messagingSenderId: '456439221439'
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(yourFirebaseConfig),
    AngularFireAuthModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
